package com.mt_ag.java.cc;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.spring.boot.starter.test.helper.AbstractProcessEngineRuleTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@SpringBootTest
@RunWith(SpringRunner.class)
@Import(MyTestConfig.class)
public class WorkflowTest extends AbstractProcessEngineRuleTest {

    @Autowired
    public RuntimeService runtimeService;

    @Autowired
    private IdentityService identityService;

    @Test
    public void shouldExecuteHappyPath() {
        String processDefinitionKey = "urlaubsantrag-process";

        identityService.setAuthenticatedUserId("user");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey);

        assertThat(processInstance).isStarted()
                .task()
                .hasDefinitionKey("urlaubBeantragen")
                .isAssignedTo("user");
        complete(task());

        assertThat(task())
                .hasDefinitionKey("urlaubPruefen")
                .isAssignedTo("admin");
        complete(task(), withVariables("urlaubGenehmigt", true));

        assertThat(processInstance).isEnded();

        HistoricActivityInstance mitarbeiterInformieren = historyService().createHistoricActivityInstanceQuery()
                .processInstanceId(processInstance.getId())
                .activityId("mitarbeiterInformieren")
                .singleResult();
        Assert.assertNotNull(mitarbeiterInformieren);
    }

}
