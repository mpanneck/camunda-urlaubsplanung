package com.mt_ag.java.cc;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

@Slf4j
@Component("urlaubsProzess")
public class UrlaubsProzess {
    public void mitarbeiterInformieren(DelegateExecution delegateExecution) {
        boolean urlaubGenehmigt = (boolean) delegateExecution.getVariable("urlaubGenehmigt");
        log.warn(urlaubGenehmigt ? "Urlaub genehmigt" : "Urlaub nicht genehmigt");
    }

    public void urlaubAnExternesSystemMelden(DelegateExecution delegateExecution) {
        log.warn("Urlaub an externes System gemeldet");
    }
}
