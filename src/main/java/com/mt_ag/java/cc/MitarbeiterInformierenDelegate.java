package com.mt_ag.java.cc;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class MitarbeiterInformierenDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        boolean urlaubGenehmigt = (boolean) delegateExecution.getVariable("urlaubGenehmigt");
        log.warn(urlaubGenehmigt ? "Urlaub genehmigt" : "Urlaub nicht genehmigt");
    }
}
